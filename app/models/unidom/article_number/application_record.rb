##
# Application record 是模块内所有模型的抽象基类。

module Unidom
  module ArticleNumber

    class ApplicationRecord < ActiveRecord::Base
      self.abstract_class = true
    end

  end
end
